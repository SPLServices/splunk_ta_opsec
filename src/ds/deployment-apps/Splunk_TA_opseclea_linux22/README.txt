Splunk Add-on for Check Point OPSEC LEA version 4.0.0
Copyright (C) 2005-2016 Splunk Inc. All Rights Reserved.

For documentation, see: http://docs.splunk.com/Documentation/AddOns/latest/OPSEC-LEA
