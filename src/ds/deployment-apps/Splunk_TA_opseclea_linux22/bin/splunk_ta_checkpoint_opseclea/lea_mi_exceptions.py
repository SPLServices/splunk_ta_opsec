
class LeaLoggrabberNotExist(Exception):
    pass


class LeaLoggrabberArgumentMissing(Exception):
    pass


class LeaGetContentError(Exception):
    pass


class LeaStartSubprocessError(Exception):
    pass


class LeaEventInvalidFormat(Exception):
    pass
