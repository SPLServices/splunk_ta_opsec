import splunktaucclib.data_collection.ta_consts as c




ignored_http_errors = [404, ]

single_arguments = ["resolve", "noresolve", "2000", "ng", "online", "no_online",
                    "auditlog", "normallog"]
pair_arguments = [c.data, c.debug_level, "configserver", c.appname,
                  "filter", "no_nagle", "conn_buf_size", "fields",
                  c.lea_server_port, c.lea_server_ip, c.lea_server_auth_port,
                  c.lea_server_auth_type, c.opsec_sslca_file, c.opsec_sic_name,
                  c.opsec_entity_sic_name]
filter_arguments = [c.start_time]

required_input_configs = [c.input_name, c.connection, c.data,
                          c.appname]

required_connection_configs = [
    c.lea_server_ip, c.lea_server_auth_port, c.opsec_sslca_file,
    c.opsec_sic_name, c.opsec_entity_sic_name]

default_log_level = 2
default_auth_type = "sslca"
