import splunktaucclib.data_collection.ta_checkpoint_manager as cp
import splunktaucclib.data_collection.ta_data_client as dc
import splunktaucclib.data_collection.ta_data_collector as tc
import lea_params as lp
import splunktaucclib.data_collection.ta_consts as c
import lea_mi_exceptions as lme
import splunktaucclib.common.log as stulog
import os
import os.path as op
import subprocess
import traceback
from threading import Thread, RLock
from time import sleep
import copy

loc_len = len("loc=")
fileid_len = len("fileid=")
time_len = len("time=")

sourcetype_dict={"audit":"opsec:audit","smartdefense":"opsec:smartdefense",
                 "vpn":"opsec:vpn","non_audit":"opsec","fw":"opsec"}
subprocess_lock = RLock()
def sequence_open(fn, *args, **kwargs):
    with subprocess_lock:
        return fn(*args, **kwargs)


def create_opseclea_data_collector(meta_configs, task_config):
    checkpoint_manager_fn = cp.TACheckPointMgr
    data_client_fn = OpsecLeaDataClient
    return tc.TADataCollector(meta_configs, task_config, checkpoint_manager_fn,
                              data_client_fn)


def get_local_file_path(file_name):
    if not file_name:
        return None
    cur_dir = op.dirname(op.dirname(op.abspath(__file__)))
    return op.join(cur_dir, "..", "local", file_name)


def get_bin_file_path(file_name):
    if not file_name:
        return None
    cur_dir = op.dirname(op.dirname(op.abspath(__file__)))
    return op.join(cur_dir, "..", "bin", file_name)


def get_logs(opsec_client):
    log_func = [stulog.logger.error, stulog.logger.warn, stulog.logger.info,
                stulog.logger.debug]
    proc = opsec_client._lea_proc
    while True:
        line = proc.stderr.readline()
        if not line:
            break
        line = line.strip('\n')
        level = lp.default_log_level
        if line.startswith("log_level="):
            end_pos = line.index(" ")
            level = line[len("log_level="):end_pos]
        log_func[int(level)](opsec_client._p + line)


def check_stop(opsec_client):
    while not opsec_client.is_stopped():
        sleep(0.5)
    if opsec_client._lea_proc.poll() is not None:
        return
    try:
        stulog.logger.info(opsec_client._p + "Going to stop")
        opsec_client._lea_proc.kill()
    except OSError as oe:
        if oe.errno == 3:
            stulog.logger.info("{} lea proc pid={} already exit"
                               .format(opsec_client._p,
                                       opsec_client._lea_proc.pid))
        else:
            stulog.logger.exception(
                "{} Failed to kill lea proc pid={}, reason:".format(
                    opsec_client._p, opsec_client._lea_proc.pid))
    else:
        stulog.logger.info("{} kill lea proc pid={} successfully".format(
            opsec_client._p, opsec_client._lea_proc.pid))


class OpsecLeaDataClient(dc.TADataClient):
    def __init__(self, meta_configs, task_configs, ckpt, ta_data_collector):
        super(OpsecLeaDataClient, self).__init__(meta_configs, task_configs,
                                                 ckpt)
        self._ckpt = {} if ckpt is None else ckpt
        self._init_arguments()
        self._stop = False
        self._total_count = 0
        self._ta_data_collector = ta_data_collector
        self._gen = self.get_contents()


    def _init_arguments(self):
        self._single_arguments = lp.single_arguments
        self._pair_arguments = lp.pair_arguments
        self._filter_arguments = lp.filter_arguments
        self._required_input_args = lp.required_input_configs
        self._required_arguments = lp.required_input_configs + lp.required_connection_configs
        self._p = self._get_log_prefix()
        self._stop = False

        log_map = {"DEBUG": 3,
                   "INFO": 2,
                   "WARN": 1,
                   "ERROR": 0
                   }

        if c.connection in self._task_configs.keys() and c.connections in self._task_configs.keys():
            connection_name = self._task_configs[c.connection]
            self._task_configs.update(
                self._task_configs[c.connections].get(connection_name))
        if c.global_settings in self._task_configs.keys():
            self._task_configs.update(self._task_configs[
                                          c.global_settings].get(c.logging))
        for arg in self._required_arguments:
            if arg not in self._task_configs.keys():
                message = self._p + "Argument {} is missing. Required arguments"
                ":{}".format(arg, self._required_arguments)
                raise lme.LeaLoggrabberArgumentMissing(message)

        self._task_configs[c.debug_level] = lp.default_log_level
        if c.level in self._task_configs.keys():
            if self._task_configs[c.level] in log_map.keys():
                self._task_configs[c.debug_level] = log_map[
                    self._task_configs[c.level]]

        if c.lea_server_auth_type not in self._task_configs.keys():
            self._task_configs[c.lea_server_auth_type] = lp.default_auth_type

        self._source = "{}:{}".format(self._task_configs[c.lea_server_ip],
                                      self._task_configs[
                                          c.lea_server_auth_port])
        self._sourcetype = sourcetype_dict[self._task_configs[c.data]]

    def _get_log_prefix(self):
        return '[{}="{}" {}="{}" {}="{}"]'.format(c.input_name,
                                                  self._task_configs[
                                                      c.input_name],
                                                  c.connection,
                                                  self._task_configs[
                                                      c.connection],
                                                  c.data,
                                                  self._task_configs[
                                                      c.data])

    def _get_arguments(self):
        args = []

        for arg in self._pair_arguments:
            if self._task_configs.get(arg):
                args.append("--{}".format(arg))
                args.append(str(self._task_configs.get(arg)))

        filter_arg = []
        for arg in self._filter_arguments:
            if arg == c.start_time and self._ckpt:
                    continue
            if self._task_configs.get(arg):
                filter_arg.append("{}={}".format(arg,
                                                 self._task_configs.get(arg)))
        if filter_arg:
            args.append("--filter")
            args.append(";".join(filter_arg))


        #set last_record_location or start_time
        if self._ckpt:
            args.append("--{}".format(c.last_record_location))
            args.append(str(self._ckpt[c.last_record_location]))


        # Check online mode or offline mode
        if c.mode in self._task_configs.keys() and self._task_configs[
            c.mode] == c.online:
            args.append("--{}".format(c.online))
        else:
            args.append("--{}".format(c.no_online))

        #resolve or no-resolve
        if c.noresolve in self._task_configs.keys() and str(self._task_configs[
            c.noresolve]) == "1":
            args.append("--no_resolve")
        else:
            args.append("--resolve")

        return args

    def is_stopped(self):
        return self._stop

    def stop(self):
        self._stop = True

    def create_message(self, data, time, ckpt=None):
        return dc.data._make((ckpt, data, time,
                              self._sourcetype,
                              self._source))



    def start_lea_loggrabber(self):
        exec_file = get_bin_file_path(c.lea_loggrabber)
        if not os.path.exists(exec_file):
            message = self._p + "{} doesn't exist!".format(exec_file)
            raise lme.LeaLoggrabberNotExist(message)
        cmd = [exec_file, ] + self._get_arguments()

        stulog.logger.info("{} Starting {}".format(self._p, " ".join(cmd)))
        try:
            self._lea_proc = sequence_open(subprocess.Popen, cmd,
                                              stdin=open(os.devnull),
                                              stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE)
        except Exception:
            message = self._p + "Failed to start subprocess, reason: {}".format(
                traceback.format_exc())
            raise lme.LeaStartSubprocessError(message)

        self._log_thd = Thread(target=get_logs, args=(self,))
        self._log_thd.start()

        self._check_stop_thd = Thread(target=check_stop, args=(self,))
        self._check_stop_thd.start()

    def get_contents(self):

        self.start_lea_loggrabber()

        # poll is to check if lea process exits or not
        while True:
            lines = self._lea_proc.stdout.readlines(1)
            if not lines:
                break
            ckpt_str = self.get_checkpoint(lines[-1])
            self._ckpt[c.last_record_location] = ckpt_str
            self._total_count += len(lines)
            yield (self.create_single_event(line) for line in lines),\
                  copy.deepcopy(self._ckpt)
        stulog.logger.info("{} Successfully indexed events: {}".format(
                           self._p, self._total_count))
        self.stop()
        self._log_thd.join()
        self._check_stop_thd.join()
        raise StopIteration

    def create_single_event(self, line):
        if not line or not line.startswith("time="):
            stulog.logger.error("invalid event format: {}".format(line))
            return None
        time, loc, fileid = self._get_meta_metric(line)
        return self.create_message(line, time)

    def get_checkpoint(self, line):
        if not line or not line.startswith("time="):
            stulog.logger.error("invalid event format: {}".format(line))
            return None
        time, loc, fileid = self._get_meta_metric(line)
        ckpt_str = self._update_ckpt(fileid, loc)
        return ckpt_str

    def get(self):
        return self._gen.next()

    def _update_ckpt(self, fileid, loc):
        ckpt = fileid + ":" +loc
        self._ckpt[c.last_record_location] = ckpt
        return ckpt

    def _get_meta_metric(self, event):
        try:
            time_end = event.index("|")
            loc_end = event[time_end+1:].index("|")
            fileid_end = event[time_end+loc_end+2:].index("|")
            return event[time_len:time_end], event[
                                  time_end+1+loc_len:time_end+1+loc_end],\
                event[time_end+2+loc_end+fileid_len:loc_end+2+fileid_end
                                                   +time_end]
        except Exception:
            message = self._p + "Current supported event should have " \
                                "time=<int>|loc=<int>|fileid=<int>, but it is {}" \
                                "".format(event)
            raise lme.LeaEventInvalidFormat(message)

