import splunktalib.modinput as modinput
from splunktaucclib.data_collection import ta_config as config
from splunktaucclib.common import load_schema_file as ld
import os.path as op
import json
from splunktalib.splunk_platform import make_splunkhome_path
from splunktalib.common import util
import splunktaucclib.data_collection.ta_consts as c


def create_ta_opseclea_config():
    meta_configs, input_configs = modinput.get_modinput_configs_from_stdin()
    client_schema = json.dumps(ld(op.join(op.dirname(op.abspath(__file__)),
                           "opsec_schema.lea_config.json")))
    return TaOpsecLeaConfig(meta_configs, input_configs, client_schema)


class TaOpsecLeaConfig(config.TaConfig):
    def _set_task_configs(self, task_configs):
        for task_config in task_configs:
            connection = task_config[c.connections][task_config[c.connection]]
            if not task_config.get(c.hostname):
                task_config[c.hostname] = connection[c.lea_server_ip]
            task_config[c.opsec_sslca_file] = self._get_opsec_sslca_file(
                connection["cert_name"])
        self._task_configs = task_configs

    def _get_opsec_sslca_file(self, cert_name):
        _app_name = util.get_appname_from_path(op.abspath(__file__))
        path = make_splunkhome_path(
            (["etc", "apps", _app_name, "certs", cert_name]))
        return path
