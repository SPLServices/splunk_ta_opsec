server_uri = "server_uri"
session_key = "session_key"
version = "version"
appname = "appname"
event_writer = "event_writer"
index = "index"
default_index = "default"
source = "source"
sourcetype = "sourcetype"
data_loader = "data_loader"
servers = "servers"
meta_configs = "meta_configs"
disabled = "disabled"
resource = "resource"
events = "events"
scope = "scope"
checkpoint_dir = "checkpoint_dir"
server_host = "server_host"
ckpt_dict = "ckpt_dict"
inputs = "inputs"
input_name = "input_name"
input_data = "input_data"
interval = "interval"
data = "data"
time_fmt = "%Y-%m-%dT%H:%M:%S"
utc_time_fmt = "%Y-%m-%dT%H:%M:%S.%fZ"

use_kv_store = "use_kv_store"
url = "url"
password = "password"
username = "username"
hostname = "hostname"
host = "host"
stanza_name = "stanza"
name = "name"
global_settings = "global_settings"
proxy_settings = "proxy_settings"
credential_settings = "credential_settings"
log_level = "log_level"
level = "level"
logging = "logging"
proxy = "proxy"

ucc_system_setting = "ucc_system_setting"



#ta related constants
ta_name = "opseclea"
ta_short_name = "opseclea"
server_prefix = "opseclea_"
client_prefix = server_prefix
ta_connection_conf = "connection"
ta_global_setting_conf = "settings"
ta_opseclea_input_conf = "inputs"
lea_loggrabber = "lea_loggrabber"
connection = "connection"
connections = "connections"
mode = "mode"
online = "online"
no_online = "no_online"
lea_server_auth_type = "lea_server_auth_type"
lea_server_auth_port = "lea_server_auth_port"
lea_server_ip = "lea_server_ip"
lea_server_port = "lea_server_port"
last_record_location = "last_record_location"
opsec_entity_sic_name = "opsec_entity_sic_name"
opsec_sic_name = "opsec_sic_name"
opsec_sslca_file = "opsec_sslca_file"
debug_level = "debug_level"
noresolve = "noresolve"
start_time = "starttime"












