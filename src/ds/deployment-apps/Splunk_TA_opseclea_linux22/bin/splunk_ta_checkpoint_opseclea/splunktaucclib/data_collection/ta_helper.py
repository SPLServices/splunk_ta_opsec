import os.path as op
import threading
import re
from datetime import datetime
from calendar import timegm
import splunktaucclib.common.log as stulog
import splunktaucclib.config as sc
from splunktalib.common import util
from splunktaucclib.data_collection import ta_consts as c
import hashlib
import json

def utc2timestamp(human_time):
    regex1 = ur"\d{4}-\d{2}-\d{2}.\d{2}:\d{2}:\d{2}"
    match = re.search(regex1, human_time)
    if match:
        formated = match.group()
    else:
        return None

    strped_time = datetime.strptime(formated, c.time_fmt)
    timestamp = timegm(strped_time.utctimetuple())

    regex2 = "\d{4}-\d{2}-\d{2}.\d{2}:\d{2}:\d{2}(\.\d+)"
    match = re.search(regex2, human_time)
    if match:
        timestamp += float(match.group(1))
    else:
        timestamp += float("0.000000")
    return timestamp

def get_md5(data):
    """
    function name is not change, actually use sha1 instead
    :param data:
    :return:
    """
    assert data is not None, "The input cannot be None"
    if isinstance(data, (unicode, str)):
        return hashlib.sha256(data.encode('utf-8')).hexdigest()
    elif isinstance(data, (list, tuple, dict)):
        return hashlib.sha256(json.dumps(data).encode('utf-8')).hexdigest()



class ConfigFileHandler(object):
    _app_name = util.get_appname_from_path(op.abspath(__file__))

    def __init__(self, meta_configs, conf_schema):
        self._lock = threading.Lock()
        self._stop = False
        self._conf = sc.Config(splunkd_uri=meta_configs[c.server_uri],
                               session_key=meta_configs[c.session_key],
                               schema=conf_schema,
                               user="nobody",
                               app=ConfigFileHandler._app_name)
        self._all_conf_content = {}
        self._load_files()

    def tear_down(self):
        with self._lock:
            self._stop = True
            sc.stop_logging()
        stulog.logger.info("Conf mgr is going to exit")

    def _load_files(self):
        try:
            self._all_conf_content = self._conf.load()
        except Exception as ex:
            with self._lock:
                if self._stop:
                    self._all_conf_content = None
                    stulog.logger.info(
                        "Conf handler is tearing down, do nothing")
                    return
            raise ex

    @staticmethod
    def is_stanza_valid(stanza, required_fields):
        for arg in required_fields:
            if not stanza.get(arg):
                stulog.logger.error(
                    "The requied fields are %s. Field '%s' is missing",
                    required_fields,
                    arg)
                return False
        return True

    def get_all_files(self, reload_file=False):
        if reload_file:
            self._load_files()
        return self._all_conf_content

    def get_one_file(self, conf_file, reload_file=False):
        if reload_file:
            self._load_files()
        return self._all_conf_content.get(conf_file)

    def get_stanza_in_file(self, stanza_name, conf_file, reload_file=False):
        file_content = self.get_one_file(conf_file, reload_file=reload_file)
        if not file_content:
            return None
        return file_content.get(stanza_name)

    def get_field(self, conf_file, stanza_name, field_name):
        stanza = self.get_stanza_in_file(stanza_name, conf_file)
        if not stanza:
            return None
        return stanza.get(field_name)

    def update_items(self, data):
        with self._lock:
            if self._stop:
                stulog.logger.info("Config File is teared down, do noting")
                return

        try:
            res = self._conf.update_items(endpoint_id=data["endpoint_id"],
                                          item_names=data["item_names"],
                                          field_names=data["field_names"],
                                          data=data["data"])
        except Exception:
            with self._lock:
                if not self._stop:
                    stulog.logger.exception("Failure during updating:")
                return data["item_names"]

        stulog.logger.debug("update conf status %s", res)
        return res

    def get_endpoints(self):
        return self._conf.get_endpoints()
