from splunktalib.common import util
import socket
import ta_consts as c
import os.path as op
import splunktalib.splunk_cluster as sc
import ta_helper as th
import splunktaucclib.common.log as stulog


# methods can be overrided by subclass : set_task_configs
class TaConfig(object):
    _current_hostname = socket.gethostname()
    _appname = util.get_appname_from_path(op.abspath(__file__))

    def __init__(self, meta_configs, input_configs, client_schema):
        self._meta_configs = meta_configs
        self._input_configs = input_configs
        self._task_configs = []
        self._server_info = sc.ServerInfo(meta_configs[c.server_uri],
                                          meta_configs[c.session_key])
        self._client_conf = th.ConfigFileHandler(meta_configs,
                                                 client_schema)
        self._generate_configs()

    def is_shc_but_not_captain(self):
        return self._server_info.is_shc_member() and \
               not self._server_info.is_captain()

    def get_meta_configs(self):
        return self._meta_configs

    def get_task_configs(self):
        return self._task_configs

    # Support two formats:
    # 1: data = data1/3600,data2/3600
    # 2: data = data1,data2 interval = 3600
    def _get_interval_dict(self, input_item):
        interval_dict = {}
        intervals = input_item[c.data].split(",")
        interval = input_item[c.interval]

        for itv in intervals:
            if not interval and itv.find("/") < 0:
                stulog.logger.error("Invalid data format:%s . Correct format: "
                                    "<data>/<interval> or missed interval "
                                    "field", itv)
                continue
            elif itv.find("/") >= 0:  # for format 1
                k, v = itv.strip().split("/")
                if k is not None and v is not None:
                    interval_dict[k] = int(v)
            else:  # for format 2
                interval_dict[itv.strip()] = int(interval.strip())

        return interval_dict

    def _get_input_name(self, input_item):
        if isinstance(input_item, basestring):
            in_name = input_item
        else:
            in_name = input_item[c.name]

        pos = in_name.find("://")
        if pos > 0:
            in_name = in_name[pos + 3:]
        return in_name

    def _generate_configs(self):
        configs = dict()
        for endpoint in self._client_conf.get_endpoints():
            configs[endpoint] = self._client_conf.get_one_file(endpoint)
        conf_content = configs.get(c.inputs)
        if not conf_content:
            return
        task_configs = []
        for input_name, input_item in conf_content.iteritems():
            if util.is_true(input_item.get(c.disabled, False)):
                stulog.logger.info("Input %s is disabled", input_name)
                continue

            interval_dict = self._get_interval_dict(input_item)
            for data in interval_dict.keys():
                if not data:
                    continue
                task = dict()
                task[c.input_name] = self._get_input_name(input_name)
                task[c.use_kv_store] = input_item.get(c.use_kv_store, False)
                task[c.appname] = TaConfig._appname
                task[c.index] = input_item.get(c.index, False)
                task[c.hostname] = input_item.get(c.host)
                if self._server_info.is_shc_member():
                    task[c.use_kv_store] = True
                task.update(self._meta_configs)
                self._add_conf_to_task(task, configs)
                task[c.data] = data
                task[c.interval] = interval_dict[data]

                for (k, v) in input_item.iteritems():
                    if not k == c.data and not k == c.interval:
                        task[k] = v

                stulog.logger.debug("Origin Task info: %s", task)
                task_configs.append(task)
        self._set_task_configs(task_configs)
        stulog.logger.info("Totally generated {} task configs".format(
            len(self._task_configs)))

    # Override this method if some transforms or validations needs to be done
    # before _task_configs is exposed
    def _set_task_configs(self, task_configs):
        self._task_configs = task_configs

    def get_log_level(self):
        global_setting = self._client_conf.get_one_file(c.global_settings)
        if not global_setting:
            return "INFO"

        logging = global_setting.get(c.logging)
        if not logging:
            return "INFO"

        return logging.get("level", "INFO")

    def _add_conf_to_task(self, task, configs):
        for k, v in configs.iteritems():
            task[k] = v
