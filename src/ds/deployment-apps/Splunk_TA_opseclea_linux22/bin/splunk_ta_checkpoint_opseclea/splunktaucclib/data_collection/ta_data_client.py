#!/usr/bin/python
import ta_helper as th
from collections import namedtuple

data = namedtuple('eventdata',
                  ['ckpt', 'event', 'index_time',
                   'sourcetype', 'source'])


class TADataClient(object):
    def __init__(self, meta_configs, task_configs, ckpt):
        self._meta_configs = meta_configs
        self._task_configs = task_configs
        self._ckpt = ckpt
        self._configs = dict()

    @staticmethod
    def get_index_time(itime):
        return th.utc2timestamp(itime)

    def get_contents(self):
        if not self._configs.get('count'):
            self._configs['count'] = 1
        elif self._configs['count'] == 10:
            raise StopIteration
        else:
            self._configs['count'] = self._configs['count'] + 1
        return data._make((self._ckpt,
                           ["test event"],
                           "index_time",
                           "test:event:sourcetype",
                           "test_event_source"))

    def get(self):
        return self.get_contents()
