#!/usr/bin/python
import traceback
import time
import ta_consts as c
import splunktaucclib.common.log as stulog
import splunktalib.common.util as scu

evt_fmt = ("<stream><event><host>{0}</host><source>{1}</source>"
           "<sourcetype>{2}</sourcetype>"
           "<time>{3}</time>"
           "<index>{4}</index><data>"
           "<![CDATA[{5}]]></data></event></stream>")


def format_event(host=None, source=None, sourcetype=None, time=None, index=None,
                 raw_data=None):
    return evt_fmt.format(host, source, sourcetype, time, index, raw_data)







class TADataCollector(object):
    def __init__(self, meta_configs, task_config, checkpoint_manager_fn,
                 data_client_fn):
        self._meta_configs = meta_configs
        self._task_config = task_config
        self._running = False
        self._stopped = False
        self._p = self._get_logger_prefix()
        self._checkpoint_manager = checkpoint_manager_fn(task_config)
        self.data_client_fn = data_client_fn

    def get_meta_configs(self):
        return self._meta_configs

    def get_task_config(self):
        return self._task_config

    def _get_logger_prefix(self):
        return '[{}="{}" {}="{}"] '.format(c.input_name,
                                          self._task_config[c.input_name],
                                          c.data,
                                          self._task_config[c.data])

    def get_interval(self):
        return self._task_config[c.interval]

    def _build_event(self, data):
        host_name = self._task_config[c.hostname]
        index = self._task_config[c.index]
        if not data:
            return None
        return (format_event(host_name, event.source,
                            event.sourcetype,
                            event.index_time,
                            index, scu.escape_cdata(event.event)) for event
               in data)


    def _get_ckpt(self):
        return self._checkpoint_manager.get_ckpt()

    def _get_ckpt_key(self):
        return self._checkpoint_manager.get_ckpt_key()

    def _update_ckpt(self, ckpt):
        return self._checkpoint_manager.update_ckpt(ckpt)

    def _create_data_client(self):
        ckpt = self._get_ckpt()
        if ckpt is None:
            metric_ckpt = None
        else:
            metric_ckpt = ckpt
        self._task_config[c.ckpt_dict] = metric_ckpt

        data_client = self.data_client_fn(self._meta_configs,
                                            self._task_config,
                                            metric_ckpt, self)

        stulog.logger.debug("{} Set {}={} for {}".format(self._p,
                            c.ckpt_dict, metric_ckpt,
                            self._task_config[c.data]))
        return data_client

    def stop(self):
        self._stopped = True
        self._client.stop()

    def __call__(self):
        self.index_data()

    def index_data(self):
        if self._running:
            return
        self._running = True
        checkpoint_key = self._get_ckpt_key()
        stulog.logger.info("{} Start indexing data for {}".format(self._p,
                           checkpoint_key))
        while not self._stopped:
            try:
                self._do_safe_index()
            except Exception:
                stulog.logger.exception("{} Failed to index data, reason:"
                                        .format(self._p))
                time.sleep(2)
                continue
            break
        self._checkpoint_manager.close()
        stulog.logger.info("{} End of indexing data for {}".format(
            self._p, checkpoint_key))

    def _write_events(self, data, ckpt):
        loader = self._task_config[c.data_loader]
        evts = self._build_event(data)
        if not loader.write_events(evts):
            stulog.logger.info("{} the event queue is closed and the "
                               "received data will be discarded".format(
                self._p))
            return False
        self.update_checkpoint(ckpt)
        return True

    def update_checkpoint(self, ckpt):
        if ckpt is None:
            return
        for i in range(3):
            try:
                self._update_ckpt(ckpt)
            except Exception:
                stulog.logger.exception(
                    "{} Failed to update ckpt {} to {}, will try again".format(
                        self._p,
                    self._get_ckpt_key(), ckpt))
                time.sleep(2)
                continue
            else:
                return
        stulog.logger.exception(
                    "{} Failed to update ckpt {} to {}".format(
                        self._p,
                    self._get_ckpt_key(), ckpt))

    def _do_safe_index(self):
        self._client = self._create_data_client()
        while not self._stopped:
            data = None
            try:
                data, ckpt = self._client.get()
            except StopIteration:
                stulog.logger.debug("{} Finished this round".format(self._p))
                break
            except Exception as e:
                error_log = ("Failed to get msg from servers={}, input={} "
                             "data={}, error={}").format(
                    self._task_config[c.hostname], self._task_config[
                        c.input_name],
                    self._task_config[c.data], traceback.format_exc())
                stulog.logger.error(self._p + error_log)
                break

            if not data:
                continue
            if not self._write_events(data, ckpt):
                break

        self._running = False


