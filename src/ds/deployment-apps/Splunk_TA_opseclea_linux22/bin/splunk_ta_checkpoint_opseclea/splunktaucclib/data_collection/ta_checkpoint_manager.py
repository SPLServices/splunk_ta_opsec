import ta_consts as c
import re
import splunktalib.state_store as ss
import splunktaucclib.common.log as stulog


class TACheckPointMgr(object):
    def __init__(self, task_configs):
        self._task_configs = task_configs
        self._store = ss.get_state_store(
            task_configs,
            task_configs[c.appname],
            use_kv_store=self._use_kv_store())

    def _use_kv_store(self):
        use_kv_store = self._task_configs.get(
            c.use_kv_store, False)
        if use_kv_store:
            stulog.logger.info("input={} Using KV store for checkpoint"
                               .format(self._task_configss[c.input_name]))
        return use_kv_store

    def get_ckpt_key(self):
        input_name = self._task_configs[c.input_name]
        input_name = re.sub("/\d+", "", input_name)
        key = "{}_{}".format(input_name, self._task_configs[c.data])
        key = key.replace('/', '_')
        return key

    def get_ckpt(self):
        key = self.get_ckpt_key()
        return self._store.get_state(key)

    def update_ckpt(self, ckpt):
        key = self.get_ckpt_key()
        self._store.update_state(key, ckpt)

    def remove_ckpt(self):
        key = self.get_ckpt_key()
        self._store.delete_state(key)

    def close(self):
        key = self.get_ckpt_key()
        self._store.close(key)



