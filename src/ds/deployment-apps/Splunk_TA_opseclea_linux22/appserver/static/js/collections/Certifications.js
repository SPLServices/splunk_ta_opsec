/*global define*/
define([
    'app/collections/ProxyBase.Collection',
    'app/models/Certification',
    'app/config/ContextMap'
], function (
    BaseCollection,
    Certification,
    ContextMap
) {
    return BaseCollection.extend({
        url: [
            ContextMap.restRoot,
            ContextMap.certification
        ].join('/'),
        model: Certification,
        initialize: function (attributes, options) {
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
