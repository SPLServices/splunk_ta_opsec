/*global define*/
define([
    'app/collections/ProxyBase.Collection',
    'app/models/Connection',
    'app/config/ContextMap'
], function (
    BaseCollection,
    Connection,
    ContextMap
) {
    return BaseCollection.extend({
        url: [
            ContextMap.restRoot,
            ContextMap.connection
        ].join('/'),
        model: Connection,
        initialize: function (attributes, options) {
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
