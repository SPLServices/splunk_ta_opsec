/*global define*/
define([
    'app/collections/ProxyBase.Collection',
    'app/models/Forwarder',
    'app/config/ContextMap'
], function (
    BaseCollection,
    Forwarder,
    ContextMap
) {
    return BaseCollection.extend({
        url: [
            ContextMap.restRoot,
            ContextMap.forwarder
        ].join('/'),
        model: Forwarder,
        initialize: function (attributes, options) {
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
