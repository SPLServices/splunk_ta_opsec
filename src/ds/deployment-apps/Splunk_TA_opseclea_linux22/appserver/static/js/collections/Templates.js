/*global define*/
define([
    'app/collections/ProxyBase.Collection',
    'app/models/Template',
    'app/config/ContextMap'
], function (
    BaseCollection,
    Template,
    ContextMap
) {
    return BaseCollection.extend({
        url: [
            ContextMap.restRoot,
            ContextMap.template
        ].join('/'),
        model: Template,
        initialize: function (attributes, options) {
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
