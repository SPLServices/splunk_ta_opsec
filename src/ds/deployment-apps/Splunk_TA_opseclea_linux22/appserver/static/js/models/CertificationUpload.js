/*global define*/
define([
    'app/models/Base.Model',
    'app/config/ContextMap'
], function (
    BaseModel,
    ContextMap
) {
    return BaseModel.extend({
        url: [
            ContextMap.restRoot,
            "ta_o365_server_certificate_upload"
        ].join('/'),

        initialize: function (attributes, options) {
            BaseModel.prototype.initialize.call(this, attributes, options);
        }
    });
});
