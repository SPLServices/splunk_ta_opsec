/*global define*/
define([
    'app/models/Base.Model',
    'app/config/ContextMap'
], function (
    BaseModel,
    ContextMap
) {
    return BaseModel.extend({
        url: [
            ContextMap.restRoot,
            ContextMap.forwarder
        ].join('/'),

        initialize: function (attributes, options) {
            BaseModel.prototype.initialize.call(this, attributes, options);
            options = options || {};
            this.collection = options.collection;

            this.addValidation('host', this.nonEmptyString);
            this.addValidation('port', this.emptyOr(this.validNumberInRange(1, 65535)));
            this.addValidation('username', this.nonEmptyString);
            this.addValidation('password', this.nonEmptyString);
        }
    });
});
