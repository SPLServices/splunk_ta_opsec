/*global define*/
define([
    'underscore',
    'app/models/Base.Model',
    'app/config/ContextMap'
], function (
    _,
    BaseModel,
    ContextMap
) {
    return BaseModel.extend({
        url: [
            ContextMap.restRoot,
            ContextMap.connection
        ].join('/'),

        initialize: function (attributes, options) {
            options = options || {};
            this.collection = options.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.addValidation('strict_name', this.validName);
            this.addValidation('lea_server_ip', this.nonEmptyString);
            this.addValidation('lea_server_auth_port', this.validNumberInRange(1, 65535));
            this.addValidation('fw_version', this.nonEmptyString);
            this.addValidation('lea_app_name', this.nonEmptyString);
            this.addValidation('one_time_password', this.nonEmptyString);
            this.addValidation('lea_server_type', this.nonEmptyString);
            this.addValidation('lea_object_name', this.validNonEmptyObjectName);
        },

        validName: function() {
            var name = this.entry.content.get('name');
            if (name && !name.match(/^[a-zA-Z0-9\-_\.]+$/)) {
                return _('Field "Name" only supports the following character: [a-z A-Z 0-9 - _ .]').t();
            }
        },

        validNonEmptyObjectName: function (attr) {
            var lea_server_type = this.entry.content.get("lea_server_type"),
                lea_object_name = this.entry.content.get(attr);
            if (lea_server_type !== 'primary' && !lea_object_name) {
                return _('Field "Log Server Object Name" is required').t();
            }
        }
    });
});
