/*global define*/
define(function () {
    return {
        "appName": "Splunk_TA_checkpoint-opseclea",
        "restRoot": "ta_opseclea",
        "connection": "ta_opseclea_cert",
        "input": "ta_opseclea_input",
        "index": "ta_opseclea_indexes",
        "setting": "ta_opseclea_settings"
    };
});
