/*global define,window*/
define([
    'underscore',
    'app/models/Account',
    'app/models/Forwarder',
    'app/views/Models/TextDisplayControl',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'views/shared/controls/SyntheticCheckboxControl',
    'views/shared/controls/SyntheticRadioControl',
    'app/views/Models/MultiSelectInputControl',
    'app/models/Connection',
    'app/models/Input',
    'app/collections/Inputs'
], function (
    _,
    Account,
    Forwarder,
    TextDisplayControl,
    TextControl,
    SingleInputControl,
    SingleInputControlEx,
    SyntheticCheckboxControl,
    SyntheticRadioControl,
    MultiSelectInputControl,
    Connection,
    Input,
    Inputs
) {
    return {
        "connection": {
            "model": Connection,
            "title": "Connection",
            "header": [
                {
                    "field": "name",
                    "label": "Name",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }

                },
                {"field": "lea_server_ip", "label": "Log Server IP", "sort": true},
                {"field": "lea_server_auth_port", "label": "Log Server Port", "sort": true},
                {"field": "fw_version", "label": "Major Version", "sort": true},
                {"field": "lea_app_name", "label": "OPSEC App Name", "sort": true},
                {
                    "field": "lea_server_type",
                    "label": "Log Server Type",
                    "sort": true,
                    mapping: function (field) {
                        var map = {
                            "primary": "Primary Management Server",
                            "secondary": "Secondary Management Server",
                            "dedicated": "Dedicated Server"
                        };
                        return map[field];
                    }
                }
            ],
            "moreInfo": [
                {
                    "field": "name",
                    "label": "Name",
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {"field": "lea_server_ip", "label": "Log Server IP"},
                {"field": "lea_server_auth_port", "label": "Log Server Port"},
                {"field": "fw_version", "label": "Major Version"},
                {"field": "lea_app_name", "label": "OPSEC App Name"},
                {"field": "lea_server_type", "label": "Log Server Type",
                    mapping: function (field) {
                        var map = {
                            "primary" : "Primary Management Server",
                            "secondary" : "Secondary Management Server",
                            "dedicated" : "Dedicated Server"
                        };
                        return map[field];
                    }
                },
                {
                    "field": "lea_object_name",
                    "label": "Log Server Object Name",
                    mapping: function (field) {
                        return field? field.replace(/</g, "&lt;").replace(/>/g, "&gt;") : "";
                    }
                }
            ],
            "entity": [
                {
                    "field": "name",
                    "label": "Name",
                    "type": TextControl,
                    "required": true
                },
                {
                    "field": "lea_server_ip",
                    "label": "Log Server IP",
                    "type": TextControl,
                    "required": true
                },
                {
                    "field": "lea_server_auth_port",
                    "label": "Log Server Port",
                    "type": TextControl,
                    "required": true,
                    "defaultValue": "18184"
                },
                {
                    "field": "fw_version",
                    "label": "Major Version",
                    "type": SingleInputControl,
                    "required": true,
                    "defaultValue": "R77",
                    "options": {
                        "disableSearch": true,
                        "autoCompleteFields": [
                            {value: "R76", label: "R76"},
                            {value: "R77", label: "R77"},
                            {value: "R80", label: "R80"}
                        ]
                    }
                },
                {
                    "field": "lea_app_name",
                    "label": "OPSEC App Name",
                    "type": TextControl,
                    "required": true
                },
                {
                    "field": "one_time_password",
                    "label": "One-time Password",
                    "type": TextControl,
                    "required": true,
                    "encrypted": true
                },
                {
                    "field": "lea_server_type",
                    "label": "Log Server Type",
                    "type": SingleInputControl,
                    "required": true,
                    "defaultValue": "primary",
                    "options": {
                        "disableSearch": true,
                        "autoCompleteFields": [
                            {value: "primary", label: "Primary Management Server"},
                            {value: "secondary", label: "Secondary Management Server"},
                            {value: "dedicated", label: "Dedicated Server"}
                        ]
                    }
                },
                {
                    "field": "lea_object_name",
                    "label": "Log Server Object Name",
                    "type": TextControl,
                    "required": true,
                    "display": "none"
                }
            ],
            "refLogic": function (model, refModel) {
                return model.entry.attributes.name === refModel.entry.content.attributes.connection;
            },
            "actions": [
                "edit",
                "delete",
                "clone"
            ],
            filterKey: ['name', 'lea_server_ip', 'lea_server_auth_port', 'fw_version', 'lea_app_name', 'lea_server_type', 'lea_object_name']
        },
        "input": {
            "title": "Input",
            "caption": {
                title: "Inputs",
                description: 'Create data inputs to collect data from OPSEC LEA.',
                enableButton: true,
                singleInput: true,
                buttonId: "addInputBtn",
                buttonValue: "Create New Input",
                enableHr: true
            },
            "header": [
                {
                    "field": "name",
                    "label": "Name",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "connection",
                    "label": "Connection",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "mode",
                    "label": "Mode",
                    "sort": true,
                    mapping: function (field) {
                        return field === "online" ? "Online Mode" : "Offline Mode";
                    }
                },
                {
                    "field": "noresolve",
                    "label": "No-Resolve",
                    "sort": true,
                    mapping: function (field) {
                        return field === "0" ? "resolve" : "no-resolve";
                    }
                },
                {
                    "field": "data",
                    "label": "Data",
                    "sort": true,
                    mapping: function (field) {
                        var map = {
                            "non_audit" : "Non-Audit",
                            "fw" : "Firewall Events",
                            "audit": "Firewall Audit",
                            "smartdefense" : "SmartDefense",
                            "vpn" : "VPN"
                        };
                        return map[field];
                    }
                },
                {
                    "field": "interval",
                    "label": "Interval",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "index",
                    "label": "Index",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "host",
                    "label": "Host",
                    "sort": true,
                    mapping: function (field) {
                        return field? field.replace(/</g, "&lt;").replace(/>/g, "&gt;") : "";
                    }
                },
                {
                    "field": "starttime",
                    "label": "Start Time",
                    "sort": true,
                    mapping: function (field) {
                        return field? field.replace(/</g, "&lt;").replace(/>/g, "&gt;") : "";
                    }
                },
                {
                    "field": "disabled",
                    "label": "Status",
                    "sort": true,
                    mapping: function (field) {
                        return field ? "Disabled" : "Enabled";
                    }
                }
            ],
            "moreInfo": [
                {
                    "field": "name",
                    "label": "Name",
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "connection",
                    "label": "Connection",
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "mode",
                    "label": "Mode",
                    mapping: function (field) {
                        return field === "online" ? "Online Mode" : "Offline Mode";
                    }
                },
                {
                    "field": "noresolve",
                    "label": "No-Resolve Mode",
                    mapping: function (field) {
                        return field === "0" ? "resolve" : "no-resolve";
                    }
                },
                {
                    "field": "data",
                    "label": "Data",
                    mapping: function (field) {
                        var map = {
                            "non_audit" : "Non-Audit",
                            "fw" : "Firewall Events",
                            "audit": "Firewall Audit",
                            "smartdefense" : "SmartDefense",
                            "vpn" : "VPN"
                        };
                        return map[field];
                    }
                },
                {
                    "field": "interval",
                    "label": "Interval",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "index",
                    "label": "Index",
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "host",
                    "label": "Host",
                    "sort": true,
                    mapping: function (field) {
                        return field? field.replace(/</g, "&lt;").replace(/>/g, "&gt;") : "</br>";
                    }
                },
                {
                    "field": "starttime",
                    "label": "Start Time",
                    mapping: function (field) {
                        return field? field.replace(/</g, "&lt;").replace(/>/g, "&gt;") : "</br>";
                    }
                },
                {
                    "field": "disabled",
                    "label": "Status",
                    "sort": true,
                    mapping: function (field) {
                        return field ? "Disabled" : "Enabled";
                    }
                }
            ],
            "services": {
                "input": {
                    "title": "OPSEC LEA",
                    "model": Input,
                    "url": "",
                    "collection": Inputs,
                    "entity": [
                        {
                            "field": "name",
                            "label": "Name",
                            "type": TextControl,
                            "required": true,
                            "help": "Unique name for the input"
                        },
                        {
                            "field": "connection",
                            "label": "Connection",
                            "type": SingleInputControl,
                            "required": true
                        },
                        {
                            "field": "mode",
                            "label": "Mode",
                            "type": SyntheticRadioControl,
                            "required": true,
                            "defaultValue": "offline",
                            "options": {
                                items: [
                                    {value: "offline", label: "Offline Mode"},
                                    {value: "online", label: "Online Mode"}
                                ]
                            }
                        },
                        {
                            "field": "noresolve",
                            "label": "No-Resolve Mode",
                            "type": SyntheticCheckboxControl,
                            "required": false
                        },
                        {
                            "field": "data",
                            "label": "Data",
                            "type": SingleInputControl,
                            "required": true,
                            "options": {
                                "disableSearch": true,
                                "autoCompleteFields": [
                                    {value: "non_audit", label: "Non-Audit"},
                                    {value: "fw", label: "Firewall Events"},
                                    {value: "audit", label: "Firewall Audit"},
                                    {value: "smartdefense", label: "SmartDefense"},
                                    {value: "vpn", label: "VPN"}
                                ]
                            }
                        },
                        {
                            "field": "interval",
                            "label": "Interval",
                            "type": TextControl,
                            "required": true,
                            "defaultValue": "3600"
                        },
                        {
                            "field": "index",
                            "label": "Index",
                            "type": SingleInputControlEx,
                            "required": true,
                            "defaultValue": "default"
                        },
                        {
                            "field": "host",
                            "label": "Host",
                            "type": TextControl,
                            "required": false
                        },
                        {
                            "field": "starttime",
                            "label": "Start Time",
                            "type": TextControl,
                            "required": false,
                            "options": {
                                "placeholder": "YYYY-MM-DDThh:mm:ssTZD"
                            }
                        }
                    ],
                    "actions": [
                        "edit",
                        "delete",
                        "enable",
                        "clone"
                    ]
                }
            },
            filterKey: ['name', 'connection', 'data', 'mode', 'noresolve', 'starttime', 'index', 'host', 'interval', 'status']
        },

        "forwarder": {
            "model": Forwarder,
            "title": "Forwarder",
            "header": [
                {
                    "field": "name",
                    "label": "Name",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "host",
                    "label": "Host",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {"field": "port", "label": "Port", "sort": true},
                {
                    "field": "username",
                    "label": "Username",
                    "sort": true,
                    mapping: function (field) {
                        return field.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    }
                },
                {
                    "field": "disabled",
                    "label": "Status",
                    "sort": true,
                    mapping: function (field) {
                        return field ? "Disabled" : "Enabled";
                    }
                }
            ],
            "entity": [
                {"field": "name", "label": "Name", "type": TextControl, "required": true},
                {"field": "host", "label": "Host", "type": TextControl, "required": true},
                {"field": "port", "label": "Port", "type": TextControl, "required": true, "defaultValue": "8089"},
                {"field": "username", "label": "Username", "type": TextControl, "required": true},
                {
                    "field": "password",
                    "label": "Password",
                    "type": TextControl,
                    "encrypted": true,
                    "associated": "username",
                    "required": true
                },
                {
                    "field": "type",
                    "label": "Type",
                    "type": TextControl,
                    "required": true,
                    "display": "hidden",
                    "defaultValue": "remote"
                }
            ],
            "actions": [
                "edit",
                "enable",
                "delete",
                "clone"
            ],
            "tag": "forwarder"
        },

        "proxy": {
            "title": "Proxy",
            "entity": [
                {"field": "proxy_enabled", "label": "Enable", "type": SyntheticCheckboxControl},
                {
                    "field": "proxy_type",
                    "label": "Proxy Type",
                    "type": SingleInputControl,
                    "options": {
                        "disableSearch": true,
                        "autoCompleteFields": [
                            {"label": "http", "value": "http"},
                            {"label": "http_no_tunnel", "value": "http_no_tunnel"},
                            {"label": "socks4", "value": "socks4"},
                            {"label": "socks5", "value": "socks5"}
                        ]
                    },
                    "defaultValue": "http"
                },
                {"field": "proxy_rdns", "label": "DNS Resolution", "type": SyntheticCheckboxControl},
                {"field": "proxy_url", "label": "Host", "type": TextControl},
                {"field": "proxy_port", "label": "Port", "type": TextControl},
                {"field": "proxy_username", "label": "Username", "type": TextControl},
                {
                    "field": "proxy_password",
                    "label": "Password",
                    "type": TextControl,
                    "encrypted": true,
                    "associated": "username"
                }
            ]
        },
        "logging": {
            "entity": [
                {
                    "field": "level",
                    "label": "Log Level",
                    "type": SingleInputControl,
                    "options": {
                        "disableSearch": true,
                        "autoCompleteFields": [
                            {"label": "INFO", "value": "INFO"},
                            {"label": "DEBUG", "value": "DEBUG"},
                            {"label": "ERROR", "value": "ERROR"}
                        ]
                    },
                    "defaultValue": "INFO"
                }
            ]
        }
    };
});
