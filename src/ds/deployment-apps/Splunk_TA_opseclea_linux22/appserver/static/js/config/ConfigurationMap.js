/*global define*/
define([], function (
) {
    return {
        "configuration": {
            "header": {
                title: "Configuration",
                description: "Configure your OPSEC LEA connection and logging level.",
                enableButton: false,
                enableHr: false
            },
            "allTabs": [
                {
                    title: "Connection",
                    order: 0,
                    active: true
                },
                {
                    title: "Logging",
                    order: 1
                }
            ]
        }
    };
});
