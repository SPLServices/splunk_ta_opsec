/*global define*/
define([
    'jquery',
    'underscore',
    'backbone',
    'app/util/Util',
    'models/Base',
    'views/shared/tablecaption/Master',
    'app/views/Component/Table',
    'app/views/Component/EntityDialog',
    'app/collections/Servers',
    'app/models/appData',
    'app/config/ComponentMap',
    'contrib/text!app/templates/Common/ButtonTemplate.html'
], function (
    $,
    _,
    Backbone,
    Util,
    BaseModel,
    CaptionView,
    Table,
    EntityDialog,
    Servers,
    appData,
    ComponentMap,
    ButtonTemplate
) {
    return Backbone.View.extend({
        initialize: function () {
            this.addonName = Util.getAddonName();
            //state model
            this.stateModel = new BaseModel();
            this.stateModel.set({
                sortKey: 'name',
                sortDirection: 'asc',
                count: 100,
                offset: 0,
                fetching: true
            });
            //accounts collection
            this.servers = new Servers([], {
                appData: {app: appData.get("app"), owner: appData.get("owner")},
                targetApp: this.addonName,
                targetOwner: "nobody"
            });

            //Change search
            this.listenTo(this.stateModel, 'change:search change:sortDirection change:sortKey', _.debounce(function () {
                this.fetchListCollection(this.servers, this.stateModel);
            }.bind(this), 0));
        },

        render: function () {
            var add_button_data = {
                    buttonId: "addServerBtn",
                    buttonValue: "Create New Server"
                },
                server_deferred = this.fetchListCollection(this.servers, this.stateModel);

            server_deferred.done(function () {
                //Caption
                this.caption = new CaptionView({
                    countLabel: _('Servers').t(),
                    model: {
                        state: this.stateModel
                    },
                    collection: this.servers,
                    noFilterButtons: true,
                    filterKey: ['name', 'key_id']
                });

                //Create view
                this.server_list = new Table({
                    stateModel: this.stateModel,
                    collection: this.servers,
                    showActions: true,
                    enableMoreInfo: true,
                    component: ComponentMap.server
                });

                this.$el.append(this.caption.render().$el);
                this.$el.append(this.server_list.render().$el);
                $('#server-tab .table-caption-inner').prepend($(_.template(ButtonTemplate, add_button_data)));

                $('#addServerBtn').on('click', function () {
                    var dlg = new EntityDialog({
                        el: $(".dialog-placeholder"),
                        collection: this.servers,
                        component: ComponentMap.server
                    }).render();
                    dlg.modal();
                }.bind(this));
            }.bind(this));

            return this;
        },

        fetchListCollection: function (collection, stateModel) {
            var search = '';
            if (stateModel.get('search')) {
                search = stateModel.get('search');
            }

            stateModel.set('fetching', true);
            return collection.fetch({
                data: {
                    sort_dir: stateModel.get('sortDirection'),
                    sort_key: stateModel.get('sortKey').split(','),
                    search: search,
                    count: stateModel.get('count'),
                    offset: stateModel.get('offset')
                },
                success: function () {
                    stateModel.set('fetching', false);
                }.bind(this)
            });
        }
    });
});
