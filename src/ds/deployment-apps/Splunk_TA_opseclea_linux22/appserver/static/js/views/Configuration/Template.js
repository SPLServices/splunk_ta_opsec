/*global define*/
define([
    'jquery',
    'underscore',
    'backbone',
    'app/util/Util',
    'models/Base',
    'views/shared/tablecaption/Master',
    'app/views/Component/Table',
    'app/views/Component/EntityDialog',
    'app/collections/Templates',
    'app/models/appData',
    'app/config/ComponentMap',
    'contrib/text!app/templates/Common/ButtonTemplate.html'
], function (
    $,
    _,
    Backbone,
    Util,
    BaseModel,
    CaptionView,
    Table,
    EntityDialog,
    Templates,
    appData,
    ComponentMap,
    ButtonTemplate
) {
    return Backbone.View.extend({
        initialize: function () {
            this.addonName = Util.getAddonName();
            //state model
            this.stateModel = new BaseModel();
            this.stateModel.set({
                sortKey: 'name',
                sortDirection: 'asc',
                count: 100,
                offset: 0,
                fetching: true
            });
            //accounts collection
            this.templates = new Templates([], {
                appData: {app: appData.get("app"), owner: appData.get("owner")},
                targetApp: this.addonName,
                targetOwner: "nobody"
            });

            //Change search, sort
            this.listenTo(this.stateModel, 'change:search change:sortDirection change:sortKey', _.debounce(function () {
                this.fetchListCollection(this.templates, this.stateModel);
            }.bind(this), 0));
        },

        render: function () {
            var add_button_data = {
                    buttonId: "addTemplateBtn",
                    buttonValue: "Create New Template"
                },
                tempalte_deferred = this.fetchListCollection(this.templates, this.stateModel);

            tempalte_deferred.done(function () {
                //Caption
                this.caption = new CaptionView({
                    countLabel: _('Templates').t(),
                    model: {
                        state: this.stateModel
                    },
                    collection: this.templates,
                    noFilterButtons: true,
                    filterKey: ['name', 'key_id']
                });

                //Create view
                this.server_list = new Table({
                    stateModel: this.stateModel,
                    collection: this.templates,
                    //refCollection: this.combineCollection(),
                    showActions: true,
                    enableMoreInfo: false,
                    component: ComponentMap.template
                });

                this.$el.append(this.caption.render().$el);
                this.$el.append(this.server_list.render().$el);
                $('#template-tab .table-caption-inner').prepend($(_.template(ButtonTemplate, add_button_data)));

                $('#addTemplateBtn').on('click', function () {
                    var dlg = new EntityDialog({
                        el: $(".dialog-placeholder"),
                        collection: this.templates,
                        component: ComponentMap.template
                    }).render();
                    dlg.modal();
                }.bind(this));
            }.bind(this));
            return this;
        },

        fetchListCollection: function (collection, stateModel) {
            var search = '';
            if (stateModel.get('search')) {
                search = stateModel.get('search');
            }

            stateModel.set('fetching', true);
            return collection.fetch({
                data: {
                    sort_dir: stateModel.get('sortDirection'),
                    sort_key: stateModel.get('sortKey').split(','),
                    search: search,
                    count: stateModel.get('count'),
                    offset: stateModel.get('offset')
                },
                success: function () {
                    stateModel.set('fetching', false);
                }.bind(this)
            });
        }
    });
});
