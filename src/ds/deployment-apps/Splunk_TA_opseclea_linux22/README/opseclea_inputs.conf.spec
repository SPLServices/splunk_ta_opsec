[<input_name>]
connection = connection name in opseclea_connection.conf
data = Data to fetch, which can be not_audit(Non-Audit), fw(Firewall Audit), ips(IPS) and vpn(VPN)
index = Index of the fetched data
interval = Input interval in seconds
mode = Input mode which can be offline or online
host = Host(Optional)
starttime = Start time for fetching data(Optional)
noresolve = Parameter of resolve or no-resolve mode(Optional)
